package uz.pdp.websocket1.model;

public enum MessageType {
    CHAT,
    CONNECT,
    DISCONNECT
}
